<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert title here</title>
</head>
<body style="background: #1cf6ad">
    <h2>订单信息展示</h2>
    <table border="1" cellspacing="0" cellpadding="0" >
        <thead>
            <th width="50px">编号</th>
            <th width="130px">地址</th>
            <th width="130px">手机号</th>
            <th width="130px">名称</th>
            <th width="130px">价格</th>
        </thead>
        <tbody>
            <c:forEach items="${tbroder}" var="t">
                <tr>
                    <td>${t.id}</td>
                    <td>${t.orderAdress}</td>
                    <td>${t.orderTelephone}</td>
                    <td>${t.orderName}</td>
                    <td>${t.totalPrice}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="TbFoodServlet">
        <input type="button" value="统计店铺收入金额"></a>
</body>
</html>
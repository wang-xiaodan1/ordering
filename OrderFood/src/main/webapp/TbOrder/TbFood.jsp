<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert title here</title>
    <base href="<%=basePath%>">
</head>
<body style="background: #1cf6ad">
<h2>统计店铺收入金额</h2>
<h3><a href="index.jsp">返回首页</a></h3>
<table border="1" cellspacing="0" cellpadding="0" >
    <thead>
    <th width="50px">编号</th>
    <th width="130px">名称</th>
    <th width="130px">价格</th>
    </thead>
    <tbody>
    <c:forEach items="${tbfood}" var="t">
        <tr>
            <td>${t.id}</td>
            <td>${t.merchantName}</td>
            <td>${t.price}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
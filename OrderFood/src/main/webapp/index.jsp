<%@ page import="java.sql.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
    <style type="text/css">
        h2{
            text-align: center;
            color: darkgreen;
        }
        div,img{
            text-align: center;
            height:520px;
        }
    </style>
</head>
<body>
<h2>欢迎来到随心所欲--在线点餐系统</h2>

<div class="container">
    <img class="background" src="images/background.jpg" alt="">
</div>

<div class="msg">
    <h4><a href="./LoginAndSignUpPage/BusinessLogin.jsp">商家请戳我>></a><br>
    <a href="./LoginAndSignUpPage/UserLogin.jsp">用户请戳我>></a><br>
        <a href="./Approval/approval.jsp">管理员请戳我>></a></h4>
    <h3>开启您的随心所欲之旅</h3>
</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>商家申请修改</title>
<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath }/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/select-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/global.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/umeditor.config.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/editor_api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/umeditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script language="javascript">
function saveButton(){
 document.forms[0].action="approvaUpdateServlet";
 document.forms[0].submit();
}
</script>

</head>

<body>
<form action="/approvaUpdateServlet">
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">商家管理</a></li>
    <li><a href="#">修改商家申请</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>商家申请</span></div>
    
    <ul class="forminfo">

        <li>
        <label>内置id</label>
        </label><input type="text" name="id" value="${approval.id}" class="dfinput" value="admin" readonly="readonly"/>
        </li>
        <li>



        <li>
            <label>店铺名称 <font color="red">*</font></label>
            <input type="text" class="dfinput"  name="storeName" value="${approval.storeName}" />
        </li>
        <li>
            <label>法人姓名 <font color="red">*</font></label>
            <input type="text" class="dfinput" value="${approval.person}"  name="person" />
        </li>
        <li>
            <label>联系电话 <font color="red">*</font></label>
            <input type="text" class="dfinput" value="${approval.numbers}"  name="numbers"/>
        </li>
        <li>
            <label>注册邮箱 <font color="red">*</font></label>
            <input type="text" class="dfinput"  value="${approval.mailbox}"   name="mailbox" />
        </li>
        <li>
            <label>密码<font color="red">*</font></label>
            <input type="text" class="dfinput" value="${approval.password}"   name="password" />
        </li>
        <li>
            <label>审批状态<font color="red">*</font></label>
            <input type="text" class="dfinput"  value="${approval.states}" name="states" />
        </li>
        <li>
            <label>店铺状态<font color="red">*</font></label>
            <input type="text" class="dfinput" value="${approval.disables}"  name="disables" />
        </li>
    <%--
      <label> <font color="red">*</font></label><textarea rows="5" cols="10" id="tomorrow_plan" required="required"   style="width: 500px; height: 30px; padding-left:5px; border: 1px solid #eaeff2; margin-top: 20px;ime-mode:disabled;"  maxlength="255">结婚</textarea></li>
    <li><label>&nbsp;</label>--%>

        <input name="" type="button" class="btn" value="确认保存" onclick="saveButton()"/></li>
    </ul>
  </div>
</form>
</body>
</html>

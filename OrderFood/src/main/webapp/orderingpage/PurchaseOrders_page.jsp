<%@ page import="java.sql.*" %>
<%@ page import="dao.BaseDao" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>订单页面</title>
    <script>
        function show2(){
            var show_part = document.querySelector('.part');
            show_part.style.display = 'block';
        }
    </script>
    <!-- Bootstrap CSS -->
    <link rel = "stylesheet"
          href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity =" sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin = "anonymous">
    <!-- jQuery first -->
    <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin = "anonymous">
    </script>
    <!-- Popper.js -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity = "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin = "anonymous">
    </script>
    <!-- Bootstrap JS -->
    <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity = "sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin = "anonymous"> </script>
</head>
<body style="background: #1cf6ad">
<%
    BaseDao ba=new BaseDao();
    Connection conn=ba.getConnection();
    Statement stmt=null;
    Statement stmt2=null;
    stmt=conn.createStatement();
    stmt2=conn.createStatement();
    String sql="SELECT * FROM tb_order";
    ResultSet rs=stmt.executeQuery(sql);
    //给该列添加了一个别名，才能在下面用
    String sql2="SELECT SUM(total_price) sum FROM tb_order;";
    ResultSet rs2=stmt2.executeQuery(sql2);
    // 这儿获取结果值得时候，传递的参数是列名
    rs2.next();
    double sum=rs2.getDouble("sum");
    System.out.println(sum);
%>
<div class="container">
    <div>
        总价为：<input type="text" name="sum" value="<%=sum%>">
    </div>
    <%
            while (rs.next()) {
                int a=rs.getInt("id");
                String b=rs.getString("order_adress");
                String c=rs.getString("order_telephone");
                String d=rs.getString("order_name");
                double e=rs.getDouble("total_price");
    %>
    <table>
        <thead>
        <tr>
            <th style="width: 20%"><b>订单号</b></th>
            <th style="width: 20%"><b>地址</b></th>
            <th style="width: 20%"><b>电话号码</b></th>
            <th style="width: 20%"><b>姓名</b></th>
            <th style="width: 20%"><b>单价</b></th>
            <th style="width: 20%"><b>操作</b></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input name="id" value="<%=a%>" ></td>
            <td><input type="text" name="order_adress" value="<%=b%>"></td>
            <td><input type="text" name="order_telephone" value="<%=c%>"></td>
            <td><input type="text" name="order_name" value="<%=d%>"></td>
            <td><input type="text" name="total_price" value="<%=e%>"></td>
            <td><a href="ordering.jsp">
                <input type="button" value="添加"></a>
                <input type="button" value="删除" onclick="del(<%=a%>)">
            </td>
        </tr>
        </tbody>
    </table>
    <%
        }
        rs.close();
        stmt.close();
        conn.close();
    %>
    <a href="DeleteOrder.jsp">
        <input type="button" value="订单结算">
    </a>
    <a href="ordering.jsp">
        <input type="button" value="返回主菜单">
    </a>
</div>
<script>
    function del(x) {
        window.location.href="DeleteFood.jsp?id="+x;
    }
</script>
</body>
</html>

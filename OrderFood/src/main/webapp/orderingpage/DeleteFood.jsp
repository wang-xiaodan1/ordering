<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="dao.BaseDao" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

          BaseDao ba=new BaseDao();
          Connection conn=ba.getConnection();
          String id=request.getParameter("id");
          String sql="DELETE FROM tb_order WHERE id=?";
          String sql2="DELETE FROM tb_pay WHERE id=?";
          PreparedStatement psmt=conn.prepareStatement(sql);
          PreparedStatement psmt2=conn.prepareStatement(sql2);
          psmt.setInt(1,Integer.parseInt(id));
          psmt.executeUpdate();
          psmt2.setInt(1,Integer.parseInt(id));
          psmt2.executeUpdate();
          response.sendRedirect("ordering.jsp");
          psmt.close();
          conn.close();
          psmt2.close();
%>
<%@ page import="java.sql.*" %>
<%@ page import="dao.BaseDao" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>订单页面</title>
    <script>
        function show2(){
            var show_part = document.querySelector('.part');
            show_part.style.display = 'block';
        }
    </script>
    <!-- Bootstrap CSS -->
    <link rel = "stylesheet"
          href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity =" sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin = "anonymous">
    <!-- jQuery first -->
    <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin = "anonymous">
    </script>
    <!-- Popper.js -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity = "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin = "anonymous">
    </script>
    <!-- Bootstrap JS -->
    <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity = "sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin = "anonymous"> </script>
</head>
<body style="background: #1cf6ad">
<%
        BaseDao ba=new BaseDao();
        Connection conn=ba.getConnection();
        Statement stmt=null;
        Statement stmt2=null;
        stmt=conn.createStatement();
        stmt2=conn.createStatement();
        String sql="SELECT * FROM tb_order";
        ResultSet rs=stmt.executeQuery(sql);
        //给该列添加了一个别名，才能在下面用
    if(!rs.next()) {
        response.sendRedirect("ordering_page.jsp");
    }
  else{
        response.sendRedirect("PurchaseOrders_page.jsp");
        }
        rs.close();
        stmt.close();
        conn.close();
%>
</body>
</html>

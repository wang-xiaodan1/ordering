<%@ page import="java.sql.*" %>
<%@ page import="dao.BaseDao" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 12:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查询食物名字</title>
</head>
<body style="background: #1cf6ad">
<h1>查询菜单</h1>
<h2><a href="ordering.jsp">
    <input type="button" value="返回主菜单">
</a></h2>
<h2><a href="PurchaseOrders.jsp">
    <input type="button" value="订单">
</a></h2>
<%
        //        连接数据库
        BaseDao ba=new BaseDao();
        Connection conn=ba.getConnection();
        Statement stmt=null;
        String foodName=request.getParameter("foodName");
        foodName = new String(foodName.getBytes("iso-8859-1"),"utf-8");
        stmt=conn.createStatement();
        String pattern="";
        if(foodName!=null && !"".equals(foodName.trim()))
        {
            pattern= new StringBuilder().append("where food_name like '").append(foodName).append("'").toString();
        }

        String sql="SELECT * FROM tb_food"+" "+pattern;
        System.out.println("sql="+sql);
        ResultSet rs=stmt.executeQuery(sql);
    while(rs.next()){
%>
<table border="1" width="80%" >
    <tr >
        <th style="width: 20%"><b>菜单号</b></th>
        <th style="width: 20%"><b>菜品类型</b></th>
        <th style="width: 20%"><b>菜品名称</b></th>
        <th style="width: 20%"><b>价格</b></th>
        <th style="width: 20%"><b>商家名称</b></th>
        <th style="width: 20%"><b>操作</b></th>
    </tr>
    <form>
        <%

            //          for(int i=1;i<=10;i++){
//                rs.next();
//              if(rs.next()){
            //这个地方退出循环了，下面的代码就没法执行了，当然就没有输出了
//                  break;
//              }
            //这个地方是用来调用结果集的
            int a=rs.getInt("id");
            String b=rs.getString("foodType_id");
            String c=rs.getString("food_name");
            double d=rs.getDouble("price");
            String e=rs.getString("merchant_name");
        %>
        <tr>
            <td><input name="id" value="<%=a%>" ></td>
            <td><input type="text" name="foodType_id" value="<%=b%>"></td>
            <td><input type="text" name="food_name" value="<%=c%>"></td>
            <td><input type="text" name="price" value="<%=d%>"></td>
            <td><input type="text" name="merchant_name" value="<%=e%>"></td>
            <td><a href="AddFood_page.jsp">
                <input type="button" value="添加">
            </a></td>
        </tr>
    </form>
</table>

<%
        }
        rs.close();
        stmt.close();
        conn.close();
%>

</body>
</html>

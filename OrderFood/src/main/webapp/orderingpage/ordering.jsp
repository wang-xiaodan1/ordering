<%@ page import="java.sql.*" %>
<%@ page import="dao.BaseDao" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 12:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>菜单页面</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel = "stylesheet"
          href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity =" sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin = "anonymous">
    <!-- jQuery first -->
    <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin = "anonymous">
    </script>
    <!-- Popper.js -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity = "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin = "anonymous">
    </script>
    <!-- Bootstrap JS -->
    <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity = "sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin = "anonymous"> </script>
</head>
<body style="background: #1cf6ad">
<div class="container">
<h1 class="text-center">菜单</h1>
<a href="PurchaseOrders.jsp">
    <input type="button" value="订单">
</a>
<%
    int currentPage=1;
    String pagination="";
    int maxResult=5;
    int offset=0;
    int totalPage=1;
    String currentPages=request.getParameter("currentPage");

    if(currentPages!=null && !"".equals(currentPages.trim()))
    {
        currentPage=Integer.parseInt(currentPages);
    }
%>
    <div class="container">
        <div class="row">
        <div class="col-md-3">
            <form action="QueryFoodType.jsp" method="post">
                菜品类型：<input type="text" name="foodType">
                <input type="submit" value="查询">
            </form>
        </div>
        <div class="col-md-3">
            <form action="QueryFoodName.jsp" method="post">
                菜品名称：<input type="text" name="foodName">
                <input type="submit" value="查询">
            </form>
        </div>
        <div class="col-md-3">
            <form action="QueryPrice.jsp" method="post">
                价格：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="price" type="text">
                <input type="submit" value="查询">
            </form>
        </div>
        <div class="col-md-3">
            <form action="QueryMerchant.jsp" method="post">
                商家名称：<input type="text" name="merchant">
                <input type="submit" value="查询">
            </form>
        </div>
        </div>
    </div>

    <%
            //        连接数据库
            BaseDao ba=new BaseDao();
            Connection conn=ba.getConnection();
            Statement stmt=null;
            String foodType=request.getParameter("foodType");
            stmt=conn.createStatement();
            String sql2="select count(*) as count from tb_food";
            ResultSet rs2=stmt.executeQuery(sql2);
            if(rs2.next())
            {
                int count=rs2.getInt("count");
                if(count%maxResult==0){
                    totalPage = count/maxResult;
                }
                else {
                    totalPage = count/maxResult+1;
                }
            }
            //纠正页数
            if(currentPage>totalPage)
            {
                currentPage = totalPage;
            }

            if(currentPage<1)
            {
                currentPage=1;
            }
            offset=(currentPage-1)*maxResult;
            pagination= new StringBuilder().append("limit").append(" ").append(offset).append(",").append(maxResult).toString();

            String sql="SELECT * FROM tb_food"+" "+pagination;
            System.out.println("sql="+sql);
            ResultSet rs=stmt.executeQuery(sql);
             while(rs.next()){
    %>
<div class="container">
    <form method="post" action="AddFood_page.jsp">
        <table class="table-layout">
            <thead>
            <tr>
                <th scope="col"><b>菜单号</b></th>
                <th scope="col"><b>菜品类型</b></th>
                <th scope="col"><b>菜品名称</b></th>
                <th scope="col"><b>价格</b></th>
                <th scope="col"><b>商家名称</b></th>
                <th scope="col"><b>操作</b></th>
            </tr>
            </thead>
                <%

                    int a=rs.getInt("id");
                    String b=rs.getString("foodType_id");
                    String c=rs.getString("food_name");
                    double d=rs.getDouble("price");
                    String e=rs.getString("merchant_name");
                %>
                <tbody>
                <tr>
                    <td><input name="id" value="<%=a%>" ></td>
                    <td><input type="text" name="foodType_id" value="<%=b%>"></td>
                    <td><input type="text" name="food_name" value="<%=c%>"></td>
                    <td><input type="text" name="price" value="<%=d%>"></td>
                    <td><input type="text" name="merchant_name" value="<%=e%>"></td>
                    <td>
                            <input type="submit" name="submit" value="添加"/>
                    </td>
                </tr>
                </tbody>
        </table>
    </form>
    </div>
<%
        }
    rs2.close();
    rs.close();
    stmt.close();
    conn.close();
%>
<%--    <script>--%>
<%--        function pce(x) {--%>
<%--            window.location.href="AddFood_page.jsp?id="+x;--%>
<%--        }--%>
<%--    </script>--%>
<div align="center">
    <a href="ordering.jsp">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="ordering.jsp?currentPage=<%=currentPage-1%>">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="ordering.jsp?currentPage=<%=currentPage+1%>">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="ordering.jsp?currentPage=<%=totalPage%>">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
</div>
</body>
</html>

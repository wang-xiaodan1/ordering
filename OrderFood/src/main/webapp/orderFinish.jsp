<%@ page import="java.sql.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: YuXi
  Date: 2022/12/8
  Time: 19:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>支付成功</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/popper.js/1.12.5/umd/popper.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="OrderForm.jsp"></script>
    <script>
        var addDetail=document.getElementById("address").innerText;
    </script>
    <script>
        window.onload=function (){
            let timer=60;
            setInterval(()=>{
                timer--;
                document.getElementById('countDown').innerText=timer;
                if(timer==0){
                    location.href="index.jsp";
                }
            },1000)
            // document.getElementsByTagName('button')[0].onclick=function (){
            //     location.href="index.jsp";
            // }
        }
    </script>
</head>
<body style="background: #1cf6ad">
<h3 style="color: forestgreen">订单付款成功!</h3>
<% Date date=new Date();
    SimpleDateFormat t=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String time=t.format(date);
%>
下单时间:<%=time%>

<%

    String JDBC_DRIVER="com.mysql.cj.jdbc.Driver";
    String DB_URL="jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8";
    String USER="root";
    String PASS="123456";
    try {
        Class.forName(JDBC_DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

        String sql = "update tb_pay set status='已支付'";
        PreparedStatement psmt=conn.prepareStatement(sql);

        int rs=psmt.executeUpdate();
        conn.close();


        psmt.close();
        conn.close();

    }

    catch (ClassNotFoundException | SQLException e) {
        e.printStackTrace();
    }
%>
<%
    Connection conn=null;
    Statement stmt=null;


    try{
        Class.forName(JDBC_DRIVER);
        conn= DriverManager.getConnection(DB_URL,USER,PASS);
        stmt=conn.createStatement();

        String sql="SELECT * FROM tb_pay";
        System.out.println("sql="+sql);
        ResultSet rs=stmt.executeQuery(sql);
%>
<%

    for(int i=1;i<=55;i++){
        rs.next();

        int a=rs.getInt("id");
        String b=rs.getString("order_adress");
        String c=rs.getString("order_telephone");

        String d=rs.getString("order_name");
        double e=rs.getDouble("total_price");
        String f=rs.getString("status");
%>
<table border="1" width="80%" >
    <tr >
        <th style="width: 20%"><b>订单号</b></th>
        <th style="width: 20%"><b>地址</b></th>
        <th style="width: 20%"><b>电话号码</b></th>
        <th style="width: 20%"><b>姓名</b></th>
        <th style="width: 20%"><b>单价</b></th>
        <th style="..."><b>状态</b></th>
    </tr>
    <form>

        <tr>
            <td><input name="id" value="<%=a%>" ></td>
            <td><input type="text" name="order_adress" value="<%=b%>"></td>
            <td><input type="text" name="order_telephone" value="<%=c%>"></td>
            <td><input type="text" name="order_name" value="<%=d%>"></td>
            <td><input type="text" name="total_price" value="<%=e%>"></td>
            <td><input type="text" name="status" value="<%=f%>"></td>
        </tr>
    </form>

</table>
<%
        }
        rs.close();
    }
    catch (ClassNotFoundException| SQLException e){
        e.printStackTrace();
    }
%>
<br>
<h4><span id="countDown" style="color: firebrick">60</span>秒后返回首页</h4>
<a href="index.jsp">
    <button>立即返回首页</button>
</a>
<a href="orderingpage/PurchaseOrders.jsp">
    <button>立即返回菜单</button>
</a>


</body>
</html>

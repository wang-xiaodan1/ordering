<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2022/12/4
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录界面</title>
    <style>
        body {
            position: relative;
            top: 0;
            background: url("../images/background.jpg") center no-repeat;
            background-size: auto 800px;
        }

        form {
            position: relative;
            text-align: center;
            margin-top: 380px;

        }
    </style>
</head>
<body>


<form action="server1.jsp" method="post">
    <div class="form-group">
        <label for="UserName"><b>用户名:</b></label>
        <input type="text" class="form-control" id="UserName" name="UserName"
               placeholder="大小写英文或数字" required>
    </div>
    <div class="form-group">
        <label for="Password"><b>密码:</b></label>
        <input type="password" class="form-control" id="Password" name="Password"
               placeholder="大小写英文或数字" required>
    </div>
    <div class="form-group">
        <label for="Phone"><b>手机号: </b></label>
        <input type="number" class="form-control" id="Phone" name="Phone"
               placeholder="数字" required>
    </div>
    <div class="form-group form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox">记住我
        </label>
    </div>


    <div class="msg">
        <button type="submit" class="btn btn-primary">登录</button>
    </div>

    <br>
    <br>

    <div class="msg">
        <a href="../Personal information and query orders/UpdateUser.jsp">信息修改</a>
    </div>


    <h5>还没注册？</h5>
    <h5><a href="UserSignUp.jsp">戳我去注册>></a></h5>


</form>


</body>
</html>

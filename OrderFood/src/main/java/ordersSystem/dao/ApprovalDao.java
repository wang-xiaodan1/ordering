package ordersSystem.dao;

import ordersSystem.connection.ConnectionFactory;
import ordersSystem.entity.Approval;
import ordersSystem.entity.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class ApprovalDao {
    
    public List<Approval> getApprovalList() {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        List<Approval> approvalList=new ArrayList<>();
        Approval approval=null;
        try {
            connection= ConnectionFactory.getConnection();
            String sql = "select * from approval   ";
            preparedStatement = connection.prepareStatement(sql);
           /* preparedStatement.setString(1, phone);*/
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                approval=new Approval();
                approval.setId(resultSet.getInt(1));
                approval.setStoreName(resultSet.getString(2));
                approval.setPerson(resultSet.getString(3));
                approval.setNumbers(resultSet.getString(4));
                approval.setMailbox(resultSet.getString(5));
                approval.setPassword(resultSet.getString(6));
                approval.setStates(resultSet.getString(7));
                approval.setDisables(resultSet.getString(8));
                approvalList.add(approval);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
        return approvalList;
    }

    public void install(Approval approval) {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;

        try {
            connection= ConnectionFactory.getConnection();
            String sql = "INSERT INTO `ordering`.`approval` (  `store_name`, `person`, `numbers`, `mailbox`, `password`, `states`, `disables` )VALUES (  ?, ?, ?, ?, ?,?, ? )";
            preparedStatement = connection.prepareStatement(sql);
             preparedStatement.setString(1, approval.getStoreName());
             preparedStatement.setString(2, approval.getPerson());
             preparedStatement.setString(3, approval.getNumbers());
            preparedStatement.setString(4, approval.getMailbox());
            preparedStatement.setString(5, approval.getPassword());
            preparedStatement.setString(6, approval.getStates());
            preparedStatement.setString(7, approval.getDisables());
            preparedStatement.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }

    }

    public Approval getById(String id) {

        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        Approval approval=null;
        try {
            connection= ConnectionFactory.getConnection();
            String sql = "select * from approval where  id = ?   ";
            preparedStatement = connection.prepareStatement(sql);
             preparedStatement.setString(1, id);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                approval=new Approval();
                approval.setId(resultSet.getInt(1));
                approval.setStoreName(resultSet.getString(2));
                approval.setPerson(resultSet.getString(3));
                approval.setNumbers(resultSet.getString(4));
                approval.setMailbox(resultSet.getString(5));
                approval.setPassword(resultSet.getString(6));
                approval.setStates(resultSet.getString(7));
                approval.setDisables(resultSet.getString(8));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
        return approval;
    }

    public void updata(Approval approval) {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;

        try {
            connection= ConnectionFactory.getConnection();
            String sql = "UPDATE `ordering`.`approval` SET `store_name` = ?, `person` = ?, `numbers` = ?, `mailbox` = ?, `password` = ?, `states` = ?, `disables` = ? WHERE `id` = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, approval.getStoreName());
            preparedStatement.setString(2, approval.getPerson());
            preparedStatement.setString(3, approval.getNumbers());
            preparedStatement.setString(4, approval.getMailbox());
            preparedStatement.setString(5, approval.getPassword());
            preparedStatement.setString(6, approval.getStates());
            preparedStatement.setString(7, approval.getDisables());
            preparedStatement.setString(8, String.valueOf(approval.getId()));
            preparedStatement.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }

    }

    public void delect(String id) {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;

        try {
            connection= ConnectionFactory.getConnection();
            String sql = "DELETE FROM approval  where id = ?";
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
    }
}

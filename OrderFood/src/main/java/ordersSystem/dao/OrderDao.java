package ordersSystem.dao;

import ordersSystem.connection.ConnectionFactory;
import ordersSystem.entity.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class OrderDao {
    public List<Order> getOrdersByphone(String phone) {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        List<Order> orders=new ArrayList<>();
        Order order=null;
        try {
            connection= ConnectionFactory.getConnection();
            String sql = "select * from tb_order where order_telephone = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, phone);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                order=new Order();
                order.setId(resultSet.getInt(1));
                order.setAddress(resultSet.getString(2));
                order.setPhone(resultSet.getString(3));
                order.setName(resultSet.getString(4));
                order.setPrice(resultSet.getDouble(5));
                order.setStatus(resultSet.getString(6));
                orders.add(order);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
        return orders;
    }
    public List<Order> getOrdersByStatus(String phone,String status) {
        Connection connection=null;
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        List<Order> orders=new ArrayList<>();
        Order order=null;
        try {
            connection= ConnectionFactory.getConnection();
            String sql = "select * from tb_pay " +
                    " where 1=1  ";
            if(phone!=null&&phone!=""){
                sql += " AND  order_telephone = '"+phone+"' ";
            }
            if(status!=null&&status!=""){
                sql += " AND status = '"+status+"'  ";
            }
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                order=new Order();
                order.setId(resultSet.getInt(1));
                order.setAddress(resultSet.getString(2));
                order.setPhone(resultSet.getString(3));
                order.setName(resultSet.getString(4));
                order.setPrice(resultSet.getDouble(5));
                order.setStatus(resultSet.getString(6));
                orders.add(order);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {

        }
        return orders;
    }
}

package ordersSystem.dao;

import ordersSystem.connection.BaseDao;
import ordersSystem.entity.TbFood;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * (TbFood)表数据库访问层
 */
public class TbFoodDao extends BaseDao {

  //商家收入统计
    public List<TbFood> tbfods(){
        String sql = "SELECT id,order_name,total_price FROM tb_pay ";
        List<TbFood> tbFoods = new ArrayList<>();
        ResultSet resultSet = executeQuery(sql, null);
        try {
            while(resultSet.next()) {
                TbFood tbFood = new TbFood(
                        resultSet.getInt("id"),
                        resultSet.getString("order_name"),
                        resultSet.getString("total_price"));
                tbFoods.add(tbFood);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                closeAll(resultSet.getStatement().getConnection()
                        , resultSet.getStatement(), resultSet);
            } catch (SQLException e) {

                e.printStackTrace();
            }
        }
        return tbFoods;
    }
}


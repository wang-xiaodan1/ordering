package ordersSystem.dao;

import ordersSystem.connection.ConnectionFactory;
import ordersSystem.entity.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    public boolean updateUserPawssword(String username,String password,String phone) {
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionFactory.getConnection();

            String sql = "update `ordering`.`users` set password=?,phone=? where username = ?";

            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, password);
            preparedStatement.setString(2, phone);
            preparedStatement.setString(3, username);

            preparedStatement.executeUpdate();
//            if(num!=-1)flag=true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean updateUserPhone(String username,String phone) {
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionFactory.getConnection();

            String sql = "update users set password=? where username = ?";

            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, phone);
            preparedStatement.setString(2, username);

            int num = preparedStatement.executeUpdate();
            if(num!=-1)flag=true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean findUserByUsername(String username) {
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionFactory.getConnection();

            String sql = "select * from users where username = ?";

            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, username);

            ResultSet resultSet= preparedStatement.executeQuery();
            flag=resultSet.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }


    public boolean findUserByUsernamePssword(String username,String password) {
        boolean flag = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = ConnectionFactory.getConnection();

            String sql = "select * from users where username = ? AND password=?";

            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            ResultSet resultSet= preparedStatement.executeQuery();
            flag=resultSet.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
}

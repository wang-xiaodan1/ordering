package ordersSystem.dao;

import ordersSystem.connection.BaseDao;
import ordersSystem.entity.TbOrder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * (TbOrder)表数据库访问层
 */
public class TbOrderDao extends BaseDao {

    //查询订单信息
    public List<TbOrder> tborders(){
        String sql = " SELECT * FROM `tb_pay` ";
        ResultSet resultSet = executeQuery(sql, null);
        List<TbOrder> lists = new ArrayList<>();
        try {
            while(resultSet.next()) {
                TbOrder tbOrder = new TbOrder(
                        resultSet.getInt("id"),
                        resultSet.getString("order_adress"),
                        resultSet.getString("order_telephone"),
                        resultSet.getString("order_name"),
                        resultSet.getString("total_price"));
                        lists.add(tbOrder);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                closeAll(resultSet.getStatement().getConnection()
                        , resultSet.getStatement(), resultSet);
            } catch (SQLException e) {

                e.printStackTrace();
            }
        }
        return lists;
    }
}


package ordersSystem.entity;

import java.io.Serializable;

/**
 * (TbOrder)实体类
 */
public class TbOrder implements Serializable {
    private static final long serialVersionUID = 848139245792053160L;
    
    private Integer id;
    /**
     * 配送地址
     */
    private String orderAdress;
    /**
     * 电话号码
     */
    private String orderTelephone;

    public TbOrder() {
    }

    /**
     * 顾客姓名
     */
    private String orderName;

    public TbOrder(Integer id, String orderAdress, String orderTelephone, String orderName, Object totalPrice) {
        this.id = id;
        this.orderAdress = orderAdress;
        this.orderTelephone = orderTelephone;
        this.orderName = orderName;
        this.totalPrice = totalPrice;
    }

    /**
     * 总价
     */
    private Object totalPrice;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderAdress() {
        return orderAdress;
    }

    public void setOrderAdress(String orderAdress) {
        this.orderAdress = orderAdress;
    }

    public String getOrderTelephone() {
        return orderTelephone;
    }

    public void setOrderTelephone(String orderTelephone) {
        this.orderTelephone = orderTelephone;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Object getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Object totalPrice) {
        this.totalPrice = totalPrice;
    }

}


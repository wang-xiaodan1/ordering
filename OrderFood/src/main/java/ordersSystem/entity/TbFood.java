package ordersSystem.entity;

import java.io.Serializable;

/**
 * (TbFood)实体类
 */
public class TbFood implements Serializable {
    private static final long serialVersionUID = 411837774069054390L;
    
    private Integer id;
    /**
     * 菜品类型
     */
    private String foodtypeId;
    /**
     * 菜品名称
     */
    private String foodName;

    public TbFood(Integer id, String price, String merchantName) {
        this.id = id;
        this.price = price;
        this.merchantName = merchantName;
    }

    /**
     * 价格¥
     */
    private String price;

    public TbFood() {
    }

    /**
     * 商家名称
     */
    private String merchantName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodtypeId() {
        return foodtypeId;
    }

    public void setFoodtypeId(String foodtypeId) {
        this.foodtypeId = foodtypeId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

}


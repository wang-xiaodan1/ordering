package ordersSystem.servlet;

import ordersSystem.entity.Approval;
import ordersSystem.entity.Order;
import ordersSystem.service.ApprovalService;
import ordersSystem.service.OrderService;
import ordersSystem.service.impl.ApprovalServiceImpl;
import ordersSystem.service.impl.OrderServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/approvalServlet")
public class ApprovalServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");

        ApprovalService approvalService=new ApprovalServiceImpl();
        List<Approval> approvalList=new ArrayList<>();
        String phone=request.getParameter("phone");
        approvalList=approvalService.getApprovalList(phone);

        request.setAttribute("approvalList",approvalList);
        request.setAttribute("phone",phone);
        RequestDispatcher dispatcher;
        dispatcher =request.getRequestDispatcher("Approval/approval.jsp");
        dispatcher.forward(request, response);
        return;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");

        System.out.println("dopost");
        OrderService orderService=new OrderServiceImpl();
        List<Order> orders=new ArrayList<>();
        String phone=request.getParameter("phone");
        String status=request.getParameter("status");
        System.out.println(phone);
        System.out.println(status);

        orders=orderService.getOrdersByStatus(phone,status);

        request.setAttribute("orders",orders);
        request.setAttribute("phone",phone);
        RequestDispatcher dispatcher;
        dispatcher =request.getRequestDispatcher("orderingpage/Orders.jsp");
        dispatcher.forward(request, response);
        return;
    }
}

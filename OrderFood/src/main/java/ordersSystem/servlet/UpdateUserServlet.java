package ordersSystem.servlet;

import ordersSystem.dao.UserDao;
import ordersSystem.entity.Order;
import ordersSystem.service.OrderService;
import ordersSystem.service.UpdateUserService;
import ordersSystem.service.impl.OrderServiceImpl;
import ordersSystem.service.impl.UpdateUserImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        String userName="";
        String newPassword="";
        String newPhone="";

        userName=request.getParameter("userName");
        newPassword=request.getParameter("newPassword");
        newPhone=request.getParameter("newPhone");
        String password = request.getParameter("password");

        UpdateUserService updateUserService=new UpdateUserImpl();
        if(new UserDao().findUserByUsernamePssword(userName,password)){
            new UserDao().updateUserPawssword(userName,newPassword,newPhone);
            request.getRequestDispatcher("LoginAndSignUpPage/UserLogin.jsp").forward(request,response);
        }else{
            out.print("<script>alert('fault!')</script>");
            return;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}

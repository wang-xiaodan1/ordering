package ordersSystem.servlet;

import ordersSystem.dao.OrderDao;
import ordersSystem.entity.Order;
import ordersSystem.service.OrderService;
import ordersSystem.service.impl.OrderServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/OrdersServlet")
public class OrdersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        request.setAttribute("orders",new OrderDao().getOrdersByStatus("",""));
        RequestDispatcher dispatcher;
        dispatcher =request.getRequestDispatcher("Personal information and query orders/Orders.jsp");
        dispatcher.forward(request, response);
        return;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        String phone=request.getParameter("phone");
        String status=request.getParameter("status");
        System.out.println(status);
        if(status.equals("1")){
            request.setAttribute("orders",new OrderDao().getOrdersByStatus("",""));
        }else{
            request.setAttribute("orders",new OrderDao().getOrdersByStatus("",status));
        }
        RequestDispatcher dispatcher;
        dispatcher =request.getRequestDispatcher("Personal information and query orders/Orders.jsp");
        dispatcher.forward(request, response);
        return;
    }
}

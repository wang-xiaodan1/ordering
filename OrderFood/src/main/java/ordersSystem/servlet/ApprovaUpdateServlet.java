package ordersSystem.servlet;

import ordersSystem.entity.Approval;
import ordersSystem.entity.Order;
import ordersSystem.service.ApprovalService;
import ordersSystem.service.OrderService;
import ordersSystem.service.impl.ApprovalServiceImpl;
import ordersSystem.service.impl.OrderServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/approvaUpdateServlet")
public class ApprovaUpdateServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        List<Approval> approvalList=new ArrayList<>();
        Approval approval = new Approval();
        approval.setId(Integer.parseInt(request.getParameter("id")));
        approval.setStoreName(request.getParameter("storeName"));
        approval.setDisables(request.getParameter("disables"));
        approval.setStates(request.getParameter("states"));
        approval.setPassword(request.getParameter("password"));
        approval.setMailbox(request.getParameter("mailbox"));
        approval.setNumbers(request.getParameter("numbers"));
        approval.setPerson(request.getParameter("person"));

        ApprovalService approvalService=new ApprovalServiceImpl();


        approvalService.updata(approval);
        approvalService.install(approval);
        approvalList=approvalService.getApprovalList(null);
        request.setAttribute("approvalList",approvalList);

        request.getRequestDispatcher("/Approval/approval.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");

        System.out.println("dopost");
        OrderService orderService=new OrderServiceImpl();
        List<Order> orders=new ArrayList<>();
        String phone=request.getParameter("phone");
        String status=request.getParameter("status");
        System.out.println(phone);
        System.out.println(status);

        orders=orderService.getOrdersByStatus(phone,status);

        request.setAttribute("orders",orders);
        request.setAttribute("phone",phone);
        RequestDispatcher dispatcher;
        dispatcher =request.getRequestDispatcher("orderingpage/Orders.jsp");
        dispatcher.forward(request, response);
        return;
    }
}

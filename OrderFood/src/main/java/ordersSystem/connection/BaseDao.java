package ordersSystem.connection;
import java.sql.*;

public class BaseDao {

    public final String URL = "jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8";
    public final String USERNAME ="root";
    public final String PASSWORD ="123456";

    private Connection connection = null;
    private PreparedStatement pstmt =null;
    private ResultSet resultSet = null;

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //数据库连接方法
    public Connection getConnection() {
        if(connection == null)
            try {
                connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return connection;
    }
    //增删改
    public int executeUpdate(String sql,Object... params) {
        try {
            pstmt = getConnection().prepareStatement(sql);
            if(params!=null && params.length>0) {
                for (int i = 0; i < params.length; i++) {
                    pstmt.setObject((i+1), params[i]);
                }
            }
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }finally {
            try {
                if(pstmt!=null)pstmt.close();
                if(connection!=null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //查询
    public ResultSet executeQuery(String sql, Object... params) {
        try {
            pstmt =getConnection().prepareStatement(sql);
            if(params!=null && params.length>0) {
                for (int i = 0; i < params.length; i++) {
                    pstmt.setObject((i+1), params[i]);
                }
            }
            resultSet = pstmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
    //关闭所有
    public void closeAll(Connection connection, Statement stmt, ResultSet resultSet) {
        try {
            if(resultSet!=null)resultSet.close();
            if(stmt!=null)stmt.close();
            if(connection!=null)connection.close();
        } catch (SQLException e) {
            System.out.println("数据库关闭异常！");
        }
    }
}

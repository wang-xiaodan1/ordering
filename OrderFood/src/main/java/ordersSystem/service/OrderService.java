package ordersSystem.service;

import ordersSystem.entity.Order;

import java.util.List;

public interface OrderService {
    List<Order> getOrdersByPhone(String phone);
    List<Order> getOrdersByStatus(String phone,String status);
}

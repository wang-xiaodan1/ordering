package ordersSystem.service;

import ordersSystem.entity.Approval;
import ordersSystem.entity.Order;

import java.util.List;

public interface ApprovalService {
    List<Approval> getApprovalList(String phone);

    void install(Approval approval);

    Approval getById(String id);

    void updata(Approval approval);

    void delect(String id);
}

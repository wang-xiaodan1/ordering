package ordersSystem.service.impl;

import ordersSystem.dao.OrderDao;
import ordersSystem.entity.Order;
import ordersSystem.service.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    @Override
    public List<Order> getOrdersByPhone(String phone) {
        OrderDao orderDao=new OrderDao();
        return orderDao.getOrdersByphone(phone);
    }

    @Override
    public List<Order> getOrdersByStatus(String phone,String status) {
        OrderDao orderDao=new OrderDao();
        if(status.equals("2")){
            return orderDao.getOrdersByStatus(phone,"已支付");
        }else if(status.equals("3")){
            return orderDao.getOrdersByStatus(phone,"待支付");
        }else{
            return orderDao.getOrdersByphone(phone);
        }
    }
}

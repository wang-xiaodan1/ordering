package ordersSystem.service.impl;

import ordersSystem.dao.ApprovalDao;
import ordersSystem.dao.OrderDao;
import ordersSystem.entity.Approval;
import ordersSystem.entity.Order;
import ordersSystem.service.ApprovalService;
import ordersSystem.service.OrderService;

import java.util.List;


public class ApprovalServiceImpl implements ApprovalService {

    @Override
    public List<Approval> getApprovalList(String phone) {
        ApprovalDao approvalDao=new ApprovalDao();
        return approvalDao.getApprovalList();
    }

    public void install(Approval approval){
        ApprovalDao approvalDao=new ApprovalDao();
        approvalDao.install(approval);
    }

    public Approval getById(String id){
        ApprovalDao approvalDao=new ApprovalDao();
      return approvalDao.getById(id);
    }

    public void updata(Approval approval){
        ApprovalDao approvalDao=new ApprovalDao();
         approvalDao.updata(approval);
    }

    public void delect(String id){
        ApprovalDao approvalDao=new ApprovalDao();
        approvalDao.delect(id);
    }


}

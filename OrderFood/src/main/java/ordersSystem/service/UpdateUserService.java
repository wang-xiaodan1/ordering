package ordersSystem.service;

import ordersSystem.entity.Order;

import java.util.List;

public interface UpdateUserService {
    void updateUser(String username,String password,String phone);
    boolean findUserByUsername(String userName);
}

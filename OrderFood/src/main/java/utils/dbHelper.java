package utils;

import java.sql.*;

public class dbHelper {
    // 声明成员变量
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    Statement statement = null;
    ResultSet resultSet = null;

    /**
     * 注册驱动连接数据库
     */
    public void getConnection(){
        try {
            // 1.注册驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("驱动注册成功！");
            // 2.连接数据库
            connection = DriverManager.getConnection
                    ("jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8","root","123456");
            System.out.println("数据库连接成功！");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 实现用户登陆，匹配数据库信息
     * @param username
     * @param password
     * @return
     */
    public int login(String username,String password){
        try {
            String sql = "select * from users where username=? and password=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,password);
            resultSet = preparedStatement.executeQuery();
            // 判断返回结果集是否有内容，也就是用户存不存在
            if (resultSet.next()){
                return 1;
            } else {
                return 2;
            }
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}


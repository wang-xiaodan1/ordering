package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDao {
    private String driver="com.mysql.cj.jdbc.Driver";
    private String user="root";
    private String password="123456";
    private String url="jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8";

    public Connection conn=null;
    public PreparedStatement ps=null;
    public ResultSet rs=null;

    public Connection getConnection(){
        if (conn==null) {
            try {
                Class.forName(driver);
                conn=DriverManager.getConnection(url, user, password);
                System.out.println("数据库连接成功");
            } catch (Exception e) {
                System.out.println("获取数据库连接对象时产生的异常"+e);
            }
        }
        return conn;
    }
    public void closeAll(Connection conn,Statement stmt,ResultSet rs,PreparedStatement pst){
        if (rs!=null) {
            try {
                rs.close();
                System.out.println("关闭数据库连接");
            } catch (Exception e) {
                System.out.println("我是关闭数据连接时产生的异常"+e);
            }
        }
        if (stmt!=null) {
            try {
                stmt.close();
                System.out.println("关闭数据库连接");
            } catch (Exception e) {
                System.out.println("我是Statement对象关闭数据库的异常"+e);
            }
        }
        if (pst!=null) {
            try {
                pst.close();
                System.out.println("关闭数据库连接");
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("我是PreparedStatement对象关闭数据库的异常"+e);
            }
        }
        if (conn!=null) {
            try {
                conn.close();
                System.out.println("关闭数据库连接");
            } catch (Exception e) {
                System.out.println("我是数据库连接对象的异常"+e);
            }
        }
    }
}



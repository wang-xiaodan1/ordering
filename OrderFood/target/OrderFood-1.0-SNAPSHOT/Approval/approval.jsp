<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>休假审批</title>
    <link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath }/css/select.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.idTabs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/select-ui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/global.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.select.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/WdatePicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/umeditor.config.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/editor_api.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/umeditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
        KE.show({
            id : 'content7',
            cssPath : './index.css'
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(e) {
            $(".select1").uedSelect({
                width : 345
            });
            $(".select2").uedSelect({
                width : 167
            });
            $(".select3").uedSelect({
                width : 100
            });
        });
    </script>
</head>

<body>

<div class="place">
    <span>位置：</span>
    <ul class="placeul">
        <li><a href="#">商家管理</a></li>
    </ul>
</div>

<!--查询条件-->
<br />
<br />

</div>

<div class="tools">

    <ul class="toolbar">
        <li class="click"><span><img src="../../images/t01.png" /></span><a href="/Approval/approvalInsert.jsp" target="_self">添加</a></li>
        <%-- <li class="click"><img src="/images/t03.png" /></span><a&lt;%&ndash; href="/pages/restmanager/restInsert.jsp"&ndash;%&gt; onclick="delCheckProduct()" target="rightFrame">删除</li>--%>
    </ul>
</div>

<table class="tablelist">
    <tbody>
    <tr>
        <td>
            <table class="tablelist">
                <tbody>
                <tr>
                    <td>
                        <table class="tablelist">
                            <tbody>
                            <tr>
                                <td>
                                    <table class="tablelist">
                                        <thead>
                                        <tr>
                                            <th><input name="id" type="checkbox" value="" checked="checked"/></th>

                                            <th width="13%">店铺名称</th>
                                            <th width="13%">法人姓名</th>
                                            <th width="13%">联系电话</th>
                                            <th width="13%">注册邮箱（账户）</th>
                                            <th width="13%">密码</th>
                                            <th width="12%">审批状态</th>
                                            <th width="12%">店铺状态</th>
                                            <th width="12%">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${approvalList}" var="report">
                                            <tr>
                                                <td><input name="ids" type="checkbox" value="${report.id}"/></td>
                                                <td id="idarray" style="display: none">111</td>
                                                <td>${report.storeName}</td>
                                                <td>${report.person}</td>
                                                <td>${report.numbers }</td>
                                                <td>${report.mailbox}</td>
                                                <td>${report.password }</td>
                                                <td>${report.states}</td>
                                                <td>${report.disables}</td>
                                                <td>
                                                    <span>
                                                        <a
                                                                href="${pageContext.request.contextPath }/approvalByIdServlet?id=${report.id}"
                                                                class="tablelink"><img
                                                                src="${pageContext.request.contextPath }/images/t02.png"/>修改
                                                        </a>
                                                        <a
                                                                href="${pageContext.request.contextPath }/approvaldelectServlet?id=${report.id}"
                                                                class="tablelink"
                                                                onclick="confirm('确定要删除吗？${users.id}111')"> <img
                                                                src="${pageContext.request.contextPath }/images/t03.png"/>删除
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>






<div class="tip">
    <div class="tiptop"><span>提示信息</span><a></a></div>

    <div class="tipinfo">
        <span><img src="images/ticon.png" /></span>
        <div class="tipright">
            <p>是否确认对信息的修改 ？</p>
            <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
        </div>
    </div>

    <div class="tipbtn">
        <input name="" type="button"  class="sure" value="确定" />&nbsp;
        <input name="" type="button"  class="cancel" value="取消" />
    </div>

</div>




</div>

<script type="text/javascript">
    $('.tablelist tbody tr:odd').addClass('odd');

</script>
<script type="text/javascript">
    function delCheckProduct() {

        //获取多选框的选中状态
        product = document.getElementsByName("ids");
        checkProduct = [];
        for (k in product) {
            if (product[k].checked) {
                checkProduct.push(product[k].value);
            }
        }
        //alert(checkProduct);

        //删除
        /*var isDel = confirm("确认删除吗？");
        if (isDel) {
            //将checkProduct数组当做参数传递到servlet
            window.location.href = "${pageContext.request.contextPath}/userDeleteByids?checkProduct=" + checkProduct;
        }
        ;*/

    }
</script>
</body>
</html>

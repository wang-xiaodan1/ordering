<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加</title>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/select-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/global.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/umeditor.config.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/editor_api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/umeditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script language="javascript">
function saveButton(){
 document.forms[0].action="approvalinstallServlet";
 document.forms[0].submit();
}
</script>

</head>

<body>
<form action="/approvalinstallServlet">
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#"></a></li>
    <li><a href="#">添加</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>商家添加</span></div>
    
    <ul class="forminfo">
   <%-- <li>
      <label>工号</label>
      </label><input type="text" class="dfinput" value="10001" readonly="readonly"/>
    </li>
    <li>
      <label>姓名</label>
      </label><input type="text" class="dfinput" value="admin" readonly="readonly"/>
    </li>--%>
    <li>
      <label>店铺名称 <font color="red">*</font></label>
      <input type="text" class="dfinput"  name="storeName" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
    </li>
   <li>
      <label>法人姓名 <font color="red">*</font></label>
      <input type="text" class="dfinput"  name="person" />
    </li>
    <li>
      <label>联系电话 <font color="red">*</font></label>
      <input type="text" class="dfinput"  name="numbers"/>
    </li>
     <li>
      <label>注册邮箱 <font color="red">*</font></label>
      <input type="text" class="dfinput"  name="mailbox" />
    </li>
       <li>
       <label>密码<font color="red">*</font></label>
       <input type="text" class="dfinput"  name="password" />
       </li>
       <li>
       <label>审批状态<font color="red">*</font></label>
       <input type="text" class="dfinput"  name="states" />
       </li>
       <li>
       <label>店铺状态<font color="red">*</font></label>
       <input type="text" class="dfinput"  name="disables" />
       </li>
       <li><label>&nbsp;</label><input name="" type="button" class="btn" value="确认保存" onclick="saveButton()"/></li>
     <%-- <li>
      <label>商家时间小计 </label>
      <input type="text" class="dfinput"  name="" />
    </li>--%>
    <li>
    </ul>
  </div>
</form>
</body>
</html>

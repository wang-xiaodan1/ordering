<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2022/12/18
  Time: 12:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商家注册页面</title>
    <style>
        body{
            position: relative;
            top:0;
            background: url("../images/background.jpg")
            center no-repeat;
            background-size: auto 800px;
        }
        form{
            position: relative;
            text-align: center;
            margin-top:450px;
        }
    </style>


</head>
<body>
<h4 text-center="center" style="color: brown">用户名或密码输入错误，请重新注册或<a href="BusinessLogin.jsp">重新登录</a></h4>
<form action="server3.jsp" method="post">
    <div class="form-group">
        <label for="ShopName"><b>店铺名称:</b></label>
        <input type="text"  class="form-control" id="ShopName" value=""
               placeholder="大小写英文或数字" required checked>
    </div>
    <div class="form-group">
        <label for="UserName1"><b>法人姓名: </b></label>
        <input type="text"  class="form-control" id="UserName1" name="UserName1"
               placeholder="大小写英文或数字" required checked>
    </div>
    <div class="form-group">
        <label for="Phone"><b>联系电话:  </b></label>
        <input type="number"  class="form-control" id="Phone" value=""
               placeholder="数字" required checked>
    </div>
    <div class="form-group">
        <label for="Email"><b>注册邮箱（账户）:</b></label>
        <input type="text"  class="form-control" id="Email" value=""
               placeholder="大小写英文,数字或@" required checked>
    </div>
    <div class="form-group">
        <label for="Password1"><b>密码:  </b></label>
        <input type="password"  class="form-control" id="Password1" name="Password1" onkeyup="checkpassword()"
               placeholder="大小写英文或数字" required checked>
    </div>
    <div class="form-group">
        <label for="Password2"><b>确认密码:</b></label>
        <input type="password"  class="form-control" id="Password2" name="Password2" onkeyup="checkpassword()"
               placeholder="再次输入密码" required checked>
    </div>

    <span id="ti shi"></span>

    <br>
    <div class="msg">
        <button class="submit">提交申请</button>
    </div>
</form>

<script type="text/javascript">
    function checkpassword() {
        var password = document.getElementById("Password1").value;
        var repassword = document.getElementById("Password2").value;

        if(password == repassword) {
            document.getElementById("ti shi").innerHTML="<br><font color='green'>两次密码输入一致</font>";
            document.getElementById("submit").disabled = false;

        }else {
            document.getElementById("ti shi").innerHTML="<br><font color='red'>两次输入密码不一致!</font>";
            document.getElementById("submit").disabled = true;
        }
    }
</script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2022/12/4
  Time: 18:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户注册页面</title>
    <style>
        body{
            position: relative;
            top:0;
            background: url("../images/background.jpg")
            center no-repeat;
            background-size: auto 800px;
        }
        form{
            position: relative;
            text-align: center;
            margin-top:450px;
        }
    </style>



</head>
<body>


<form action="server4.jsp" method="post">
    <div class="form-group">
        <label for="phone1"><b>手机号:  </b></label>
        <input type="number"  class="form-control" id="phone1" name="phone1"
               placeholder="数字" required>
    </div>
    <div class="form-group">
        <label for="name1"><b>用户名:  </b></label>
        <input type="text"  class="form-control" id="name1" name="name1"
               placeholder="大小写英文或数字" required>
    </div>
    <div class="form-group">
        <label for="pass_word"><b>密码:  </b></label>
        <input type="password"  class="form-control" id="pass_word" name="pass_word" onkeyup="checkpassword()"
               placeholder="大小写英文或数字" required>
    </div>
    <div class="form-group">
        <label for="pass_word1"><b>确认密码:  </b></label>
        <input type="password"  class="form-control" id="pass_word1" name="pass_word1" onkeyup="checkpassword()"
               placeholder="再次输入密码" required>
    </div>
    <span id="ti shi"></span>
    <div class="msg">
        <button type="submit" class="btn btn-primary">提交</button>
    </div>
    <div class="msg">
        <h4>返回继续<a href="UserLogin.jsp">登录</a></h4>
    </div>
</form>

<script type="text/javascript">
    function checkpassword() {
        var password = document.getElementById("pass_word").value;
        var repassword = document.getElementById("pass_word1").value;

        if(password == repassword) {
            document.getElementById("ti shi").innerHTML="<br><font color='green'>两次密码输入一致</font>";
            document.getElementById("submit").disabled = false;

        }else {
            document.getElementById("ti shi").innerHTML="<br><font color='red'>两次输入密码不一致!</font>";
            document.getElementById("submit").disabled = true;
        }
    }
</script>
</body>

</html>

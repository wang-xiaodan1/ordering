<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2022/12/4
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录界面</title>
    <style>
        body{
            position: relative;
            top:0;
            background: url("../images/background.jpg")
            center no-repeat;
            background-size: auto 800px;
        }

        form{
            position: relative;
            text-align: center;
            margin-top:380px;

        }
    </style>
</head>
<body>

<form action="server2.jsp" method="post">
    <div class="form-group">
        <label for="UserName1"><b>用户名:</b></label>
        <input type="text"  class="form-control" id="UserName1" name="Username"
               placeholder="大小写英文或数字" required checked>
    </div>
    <div class="form-group">
        <label for="Password1"><b>密码:</b></label>
        <input type="password"  class="form-control" id="Password1" name="Password1"
               placeholder="大小写英文或数字" required checked>
    </div>
    <div class="form-group form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox">记住我
        </label>
    </div>

    <div class="msg">
        <button type="submit" class="btn btn-primary">登录</button>
    </div>
    <h5>登录不了？</h5>
    <h5><a href="BusinessSignUp.jsp">戳我去注册>></a></h5>


</form>

</body>
</html>

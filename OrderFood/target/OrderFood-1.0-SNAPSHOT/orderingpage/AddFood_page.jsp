<%@ page import="java.sql.*" %>
<%@ page import="dao.BaseDao" %>

<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/11/27
  Time: 9:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>加入购物车页面</title>
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel = "stylesheet"
          href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity =" sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin = "anonymous">
    <!-- jQuery first -->
    <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin = "anonymous">
    </script>
    <!-- Popper.js -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity = "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin = "anonymous">
    </script>
    <!-- Bootstrap JS -->
    <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity = "sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin = "anonymous"> </script>
</head>
<body style="background: #1cf6ad">
<div class="container">
    <a href="ordering.jsp"><input type="submit" value="返回菜单"></a>
    <form action="AddFood.jsp" class="text-center">
        <br>
        <br>
        <div>餐号：<input name="id" type="text" ></div>
        <br>
          <div>地址：
            <input type="text" name="order_adress" placeholder="河南省" required>
        </div>
        <br>
        <div>电话：<input type="text" name="order_telephone"></div>
        <br>
        <div>姓名：<input type="text" name="order_name"></div>
        <br>
        <%

            BaseDao ba=new BaseDao();
            Connection conn=ba.getConnection();
            String id=request.getParameter("id");
            Statement stmt = conn.createStatement();
            String sql="SELECT price FROM tb_food WHERE id="+id;
            System.out.println("sql="+sql);
            ResultSet rs=stmt.executeQuery(sql);
            rs.next();
            double pce=rs.getDouble("price");
        %>
        <div>价格：<input type="text" name="total_price" value="<%=pce%>"></div>
        <br>
        <button type="submit" class="btn" id="btn1">添加至订单</button>
    </form>
</div>
</center>
</body>
</html>

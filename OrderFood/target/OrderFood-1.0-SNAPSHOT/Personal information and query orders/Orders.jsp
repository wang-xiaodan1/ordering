<%--
  Created by IntelliJ IDEA.
  User: administrator
  Date: 2022/12/11
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
    <style type="text/css">
        body{
            background: #1cf6ad;
            display: flex;

        }
        .box{

            width: 1200px;
            margin: 0 auto;
            margin-top: 100px;
        }
        .box table{
            width: 100%;
            color: white;
            border-collapse: collapse;
        }
        .box th{
            height: 50px;
            background:#17a2b8 ;
        }
        .box td{
            background: #dadada;
            color: black;
            height: 50px;
        }
        #subbmit{
            width: 100px;
            height: 30px;
            background: grey;
            border: none;
            border-radius:10px ;
            color: white;
        }
        .text-white{
            color: white;
        }
    </style>
</head>
<body>
<div class="box">
    <form method="post" action="/OrdersServlet">
        <span class="text-white">全部:</span>
        <input type="radio" name="status" value="1" checked="true"/>
        <span class="text-white">已支付:</span>
        <input type="radio" name="status" value="已支付"/>
        <span class="text-white">待支付:</span>
        <input type="radio" name="status" value="待支付"/>
        <button type="submit" id="subbmit">查询</button>
    </form>
    <table style="margin-top: 10px;">
        <thead align="center">
        <tr>
            <th scope="col">id</th>
            <th scope="col">地址</th>
            <th scope="col">手机号</th>
            <th scope="col">名称</th>
            <th scope="col">价格</th>
            <th scope="col">状态</th>
        </tr>
        </thead>
        <tbody align="center">
        <c:forEach var="order" items="${orders}">
            <tr>
                <td>${order.id}</td>
                <td>${order.address}</td>
                <td>${order.phone}</td>
                <td>${order.name}</td>
                <td>${order.price}</td>
                <td>${order.status}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>

<%--<%@ page import="java.sql.*" %>&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: administrato--%>
<%--  Date: 2022/12/11--%>
<%--  Time: 20:08--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <title>Title</title>--%>
<%--    <style type="text/css">--%>
<%--        body{--%>
<%--            background: #1cf6ad;--%>
<%--            display: flex;--%>

<%--        }--%>
<%--        .box{--%>

<%--            width: 1200px;--%>
<%--            margin: 0 auto;--%>
<%--            margin-top: 100px;--%>
<%--        }--%>
<%--        .box table{--%>
<%--            width: 100%;--%>
<%--            color: white;--%>
<%--            border-collapse: collapse;--%>
<%--        }--%>
<%--        .box th{--%>
<%--            height: 50px;--%>
<%--            background:#17a2b8 ;--%>
<%--        }--%>
<%--        .box td{--%>
<%--            background: #dadada;--%>
<%--            color: black;--%>
<%--            height: 50px;--%>
<%--        }--%>
<%--        #subbmit{--%>
<%--            width: 100px;--%>
<%--            height: 30px;--%>
<%--            background: grey;--%>
<%--            border: none;--%>
<%--            border-radius:10px ;--%>
<%--            color: white;--%>
<%--        }--%>
<%--        .text-white{--%>
<%--            color: white;--%>
<%--        }--%>
<%--    </style>--%>
<%--</head>--%>
<%--<body>--%>

<%--<%--%>
<%--    String JDBC_DRIVER="com.mysql.cj.jdbc.Driver";--%>
<%--    String DB_URL="jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8";--%>
<%--    String USER="root";--%>
<%--    String PASS="123456";--%>

<%--    Connection conn=null;--%>
<%--    Statement stmt=null;--%>


<%--    try{--%>
<%--        Class.forName(JDBC_DRIVER);--%>
<%--        conn= DriverManager.getConnection(DB_URL,USER,PASS);--%>
<%--        stmt=conn.createStatement();--%>

<%--        String sql="SELECT * FROM tb_pay";--%>
<%--        System.out.println("sql="+sql);--%>
<%--        ResultSet rs=stmt.executeQuery(sql);--%>
<%--%>--%>

<%--<div class="box">--%>
<%--    <form method="post" action="/OrdersServlet">--%>
<%--        <input name="phone" value="${phone}"style="display:none;">--%>
<%--        <span class="text-white">全部:</span>--%>
<%--        <input type="radio" name="status" value="1" checked="true"/>--%>
<%--        <span class="text-white">已支付:</span>--%>
<%--        <input type="radio" name="status" value="2"/>--%>
<%--        <span class="text-white">未支付:</span>--%>
<%--        <input type="radio" name="status" value="3"/>--%>
<%--        <button type="submit" id="submit">查询</button>--%>
<%--    </form>--%>
<%--    <%--%>

<%--        for(int i=1;i<=55;i++){--%>
<%--            rs.next();--%>

<%--            int a=rs.getInt("id");--%>
<%--            String b=rs.getString("order_adress");--%>
<%--            String c=rs.getString("order_telephone");--%>

<%--            String d=rs.getString("order_name");--%>
<%--            double e=rs.getDouble("total_price");--%>
<%--            String f=rs.getString("status");--%>
<%--    %>--%>
<%--    <table>--%>
<%--        <thead align="center">--%>
<%--        <tr>--%>
<%--            <th scope="col">id</th>--%>
<%--            <th scope="col">地址</th>--%>
<%--            <th scope="col">手机号</th>--%>
<%--            <th scope="col">名称</th>--%>
<%--            <th scope="col">价格</th>--%>
<%--            <th scope="col">状态</th>--%>
<%--        </tr>--%>
<%--        </thead>--%>
<%--        <tbody align="center">--%>
<%--        <tr>--%>
<%--            <td><input name="id" value="<%=a%>" ></td>--%>
<%--            <td><input type="text" name="order_adress" value="<%=b%>"></td>--%>
<%--            <td><input type="text" name="order_telephone" value="<%=c%>"></td>--%>
<%--            <td><input type="text" name="order_name" value="<%=d%>"></td>--%>
<%--            <td><input type="text" name="total_price" value="<%=e%>"></td>--%>
<%--            <td><input type="text" name="status" value="<%=f%>"></td>--%>
<%--        </tr>--%>
<%--        </tbody>--%>
<%--    </table>--%>
<%--</div>--%>
<%--</body>--%>
<%--<%--%>
<%--        }--%>
<%--        rs.close();--%>
<%--    }--%>
<%--    catch (ClassNotFoundException| SQLException e){--%>
<%--        e.printStackTrace();--%>
<%--    }--%>
<%--%>--%>
<%--</html>--%>




<%@ page import="java.sql.*" %><%--
  Created by IntelliJ IDEA.
  User: YuXi
  Date: 2022/12/7
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>支付页面</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/popper.js/1.12.5/umd/popper.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="resource/js/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var totalTime = parseFloat($('#minute').text()), totalSecond = totalTime * 60, minute = $('#minute'), second = $('#second');
            function setTimeover() {
                window.setInterval(function() {
                    if (totalSecond > 0) {
                        totalSecond--;
                    } else {
                        minute.text(0);
                        second.text(0);
                        location.href="orderingpage/PurchaseOrders.jsp";
                        return false;

                    }
                    var minuteTime = Math.floor(totalSecond / 60), secondTime = totalSecond % 60;
                    minute.text(minuteTime);
                    second.text(secondTime);
                }, 1000);
            }
            setTimeover();


            $('#pay').click(function (){
                var flag=window.confirm("是否确认支付?");
                if(flag){
                    location.href="orderFinish.jsp";
                }
            })

        });

    </script>
    <%--<script>
        window.setTimeout("<jsp:forward page="PurchaseOrders.jsp"></jsp:forward>",60000);
    </script>--%>
</head>
<body style="background: #1cf6ad">
<h1 class="text-center">支付详情</h1>
<a href="#" class="dropdown-toggle">
    订单待支付有效时间:<i id="minute">3</i>分<i id="second">0</i>秒
</a>


<%
    String JDBC_DRIVER="com.mysql.cj.jdbc.Driver";
    String DB_URL="jdbc:mysql://localhost:3306/ordering?serverTimezone=GMT%2B8&characterEncoding=utf-8";
    String USER="root";
    String PASS="123456";

    Connection conn=null;
    Statement stmt=null;


    try{
        Class.forName(JDBC_DRIVER);
        conn= DriverManager.getConnection(DB_URL,USER,PASS);
        stmt=conn.createStatement();

        String sql="SELECT * FROM tb_pay where status='待支付'";
        System.out.println("sql="+sql);
        ResultSet rs=stmt.executeQuery(sql);
    %>
<%

    for(int i=1;i<=55;i++){
        rs.next();

        int a=rs.getInt("id");
        String b=rs.getString("order_adress");
        String c=rs.getString("order_telephone");

        String d=rs.getString("order_name");
        double e=rs.getDouble("total_price");
        String f=rs.getString("status");
%>
<table border="1" width="80%" >
    <tr >
        <th style="width: 20%"><b>订单号</b></th>
        <th style="width: 20%"><b>地址</b></th>
        <th style="width: 20%"><b>电话号码</b></th>
        <th style="width: 20%"><b>姓名</b></th>
        <th style="width: 20%"><b>单价</b></th>
        <th style="..."><b>状态</b></th>
    </tr>
    <form>

        <tr>
            <td><input name="id" value="<%=a%>" ></td>
            <td><input type="text" name="order_adress" value="<%=b%>"></td>
            <td><input type="text" name="order_telephone" value="<%=c%>"></td>
            <td><input type="text" name="order_name" value="<%=d%>"></td>
            <td><input type="text" name="total_price" value="<%=e%>"></td>
            <td><input type="text" name="status" value="<%=f%>"></td>
        </tr>
    </form>

</table>
<%
        }
        rs.close();
    }
    catch (ClassNotFoundException| SQLException e){
        e.printStackTrace();
    }
%>
<br>
<form action="#">
    <div>
        <label for="address">备注信息:</label>
        <textarea name="address" id="address" cols="100" rows="1"></textarea>
    </div>
</form>

<a href="orderingpage/PurchaseOrders.jsp">
<input type="button" value="取消支付">
</a>

<input type="button" id="pay" value="确认支付">
<a href="OrdersServlet">
    <input type="button" value="查看支付订单">
</a>
</html>
